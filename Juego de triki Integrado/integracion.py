import random

class Tablero:    
    def __init__(self):
        #definir nuestra matriz 3x3 
        self.matriz=[
            ["","",""],
            ["","",""],
            ["","",""]
            ]
         
    def verificar_Jugada (self, x, y):
        if x==-1:
            return False
        if self.matriz[x][y] =="":
            return True
        else:
            return False  
   
    def verificarColumnas(self):
        i=0
        while(i<3):
            if self.matriz[0][i]==self.matriz[1][i] and self.matriz[0][i]==self.matriz[2][i]:
                triqui=True
                i=i+1
            return True
              
    def verificarFilas(self,):
        i=0
        while(i<3):
            if self.matriz[i][0]==self.matriz[i][1] and self.matriz[i][0]==self.matriz[i][2]:
                triqui=True
                i=i+1       
            return True    

    def verificarDiag(self):
        if self.matriz[0][0]==self.matriz[1][1] and self.matriz[0][0]==self.matriz[2][2]:
            return True 
        elif self.matriz[0][2]==self.matriz[1][1] and self.matriz[0][2]==self.matriz[2][0]:
            return  True
        #return True 
                
    def verificar_triqui(self):
        self.matriz=False
        self.matriz=self.verificarFilas()
        if self.matriz==False:
            self.matriz=self.verificarColumnas()
            if self.matriz==False:
                self.matriz=self.verificarDiag()          
        return self.matriz
    
    def mostrar_tablero(self):
        print(self.matriz)
    
class Ficha:    
    def __init__(self):        
        self.simbolo=""

    def set_simbolo(self,valor):
        self.simbolo=valor
                
class Jugador:
    def __init__(self, valor):
        self.miFicha=Ficha()
        self.tipo=valor
   
    def realizar_jugada(self,untablero):   
        x=-1
        y=0
        while untablero.verificar_Jugada(x,y)==False:
         if self.tipo=="humano":
            x=int(input("Ingrese una fila: "))
            y=int(input("Ingrese una columna: "))
         else:
            x=random.randint(0,2)
            y=random.randint(0,2)
        untablero.matriz[x][y]=self.miFicha.simbolo
        return untablero

    def seleccionar_simbolo(self):
        x=random.randint(0,1)
        if x==0:
            self.miFicha.simbolo="O"
        else:
            self.miFicha.simbolo="X" 
          
class Juego:
    def __init__(self):
        #Primer jugador         
         self.miJugador=Jugador("humano")
        #segundo jugador
         self.Computador=Jugador("computador")
         self.miTablero= Tablero()
    
    def jugarTriqui(self):
       
        self.miJugador.seleccionar_simbolo()

        if self.miJugador.miFicha.simbolo=="X":
            self.Computador.miFicha.simbolo="O"
        else:
            self.Computador.miFicha.simbolo="X"  

        jugadas=0

        while jugadas<9:
            self.miJugador.realizar_jugada(self.miTablero)  
            if self.miTablero.verificar_triqui():
                self.miTablero.mostrar_tablero()
                print("Ganador")
                return True
            self.Computador.realizar_jugada(self.miTablero)
            if self.miTablero.verificar_triqui():
                self.miTablero.mostrar_tablero()
                print("Perdedor")
                return True
            jugadas=jugadas+1
           
miJuego=Juego()
miJuego.jugarTriqui()        
  
             